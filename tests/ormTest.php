<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';

class ormTest extends PHPUnit_Framework_TestCase
{


    public function testRecordCreation()
    {
    	$set = new ORM_MockSet();
    	$record = $set->newRecord();
    	
    	$this->assertInstanceOf('ORM_Mock', $record);
    	
    	return $record;
    }



    public function testArrayAccess()
    {
    	$set = new ORM_MockSet();
    	$record = $set->newRecord();
    	$record->save();
    	
    	$fields = $set->getFields();
    	$values = $record->getValues();
    	
    	$this->assertEquals(count($fields), count($values));
    }



    /**
     * @param ORM_Record $record
     * @depends testRecordCreation
     */
    public function testParentSetAfterRecordCreation($record)
    {
        $parentSet = $record->getParentSet();
        $this->assertInstanceOf('ORM_MockSet', $parentSet);
    }


    /**
     * @param ORM_Record $record
     * @depends testRecordCreation
     */
    public function testGetRecordTitle($record)
    {
        $title = $record->getRecordTitle();
        $this->assertEquals('', $title);
        
        $record->string = 'my title';
        $title = $record->getRecordTitle();
        $this->assertEquals('my title', $title);
    }

    
    
    public function testSetTableName()
    {
        global $babDB;

        $regular = ORM_MySqlBackend::computeTableName('addon_RecordSet');
        $namespaced = ORM_MySqlBackend::computeTableName('Ovidentia\\Addon\\RecordSet');

        $this->assertEquals('addon_record', $regular);
        $this->assertEquals('ovidentia_addon_record', $namespaced);
    }
}
