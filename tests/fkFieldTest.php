<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/fieldTest.php';

class ORM_fkFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_FkField';




    /**
     * @return ORM_FkField
     */
    protected function construct()
    {
        $args = func_get_args();
        if (!isset($args[1])) {
            $args[1] = 'testSet';
        }
        return parent::construct($args[0], $args[1]);
    }




    /**
     *
     */
    public function testNewField()
    {
        // Creates a Mock_ORM_Field.
        $field = $this->construct('name', 'testSet');

        $this->assertInstanceOf($this->fieldClass, $field);
    }



    /**
     * @expectedException ORM_Exception
     * @expectedExceptionMessage FkTestSet
     */
    public function testForeignSetWithoutClass()
    {
        $set = new ORM_MockSet();

        $field = $this->construct('fkname', 'FkTestSet');
        $set->addFields($field);

        $record = $set->newRecord();
        $record->fkname = 666;
        $record->fkname();
    }


    public function testForeignSetCreateSetNoRecord()
    {
        $set = new ORM_MockSet();

        $field = $this->construct('fkname', 'ORM_MockSet');
        $set->addFields($field);

        $record = $set->newRecord();
        $record->fkname = 666;
        $record = $record->fkname();

        $this->assertNull($record);
    }


    public function testForeignSetWithMockRecord()
    {
        $set = new ORM_MockSet();
        $foreignSet = new ORM_MockSet();
        $mockBackend = $set->getBackend();

        $field = $this->construct('fkname', 'ORM_MockSet');
        /*@var $field ORM_FkField */
        $field->setForeignSet($foreignSet);
        $set->addFields($field);

        $record = $set->newRecord();
        $record->fkname = '666';

        $foreignRecord = $foreignSet->newRecord();
        $foreignRecord->id = '666';

        $mockBackend->setSelectReturn($foreignSet, $foreignSet->id->is($record->fkname), array($foreignRecord));

        $record = $record->fkname();

        $this->assertInstanceOf('ORM_Mock', $record);
        $this->assertEquals('666', $record->id);
    }




    public function testJoiningForeignSetUsingJoinMethod()
    {
        $set = new ORM_MockSet();
        $foreignSetClassName = 'ORM_MockSet';
        $field1 = new ORM_FkField('fk', $foreignSetClassName);
        $set->addFields($field1);

        $fk = $set->join('fk');

        $this->assertInstanceOf('ORM_MockSet', $fk);
    }


    public function testJoiningForeignSetUsingMagicCallMethod()
    {
        $set = new ORM_MockSet();
        $foreignSetClassName = 'ORM_MockSet';
        $field1 = new ORM_FkField('fk', $foreignSetClassName);
        $set->addFields($field1);

        $set->fk();

        $this->assertInstanceOf('ORM_MockSet', $set->fk);
    }


    /**
     * After changing the foreign set on a fkField, joining this
     * fkField using the join() method MUST return the same record set.
     */
    public function testChangingForeignSetHasEffectOnJoinMethod()
    {
        $set = new ORM_MockSet();
        $set->hasOne('fk', 'ORM_MockSet');

        $newRecordSet = new ORM_TestRecordSet();
        $set->fk->setForeignSet($newRecordSet);

        $set->join('fk');

        $this->assertSame(
            $set->fk,
            $newRecordSet,
            'After changing the foreign set on a fkField using setForeignSet, joining this fkField using the join() method MUST return the same record set'
        );
    }


    /**
     * After changing the foreign set on a fkField, joining this
     * fkField using the join() method MUST return the same record set.
     */
    public function testChangingForeignSetHasEffectOnMagicCallMethod()
    {
        $set = new ORM_MockSet();
        $set->hasOne('fk', 'ORM_MockSet');

        $newRecordSet = new ORM_TestRecordSet();
        $set->fk->setForeignSet($newRecordSet);

        $set->fk();

        $this->assertSame(
            $set->fk,
            $newRecordSet,
            'After changing the foreign set on a fkField using setForeignSet, joining this fkField using the __call() method MUST return the same record set'
        );
    }




    private function getForeignSet()
    {
        global $babDB;

        // use backend on test database
        $babDB = new LibOrm_MysqlAdapter('localhost', 'test', '', 'test');
        $backend = new ORM_MySqlBackend($babDB);

        $set = new ORM_TestRecordSet();
        $set->setTableName('mainTable');
        $set->setBackend($backend);
        $set->setPrimaryKey('id');
        $set->addFields(
            ORM_StringField('name'),
            ORM_FkField('fk', 'ORM_TestRecordSet')
        );


        $newRecordSet = new ORM_TestRecordSet();
        $newRecordSet->setname('fk');
        $newRecordSet->setTableName('joinedTable');
        $newRecordSet->setBackend($backend);
        $newRecordSet->setPrimaryKey('id');
        $newRecordSet->addFields(ORM_StringField('name'));

        $set->fk->setForeignSet($newRecordSet);

        require_once dirname(__FILE__) . '/../vendor/ovidentia/ovidentia/ovidentia/utilit/devtools.php';
        $synch = new bab_synchronizeSql();
        $synch->addOrmSet($set);
        $synch->addOrmSet($newRecordSet);
        $synch->updateDatabase();

        return $set;
    }



    /**
     * @expectedException ORM_BackEndSaveException
     */
    public function testSavingForeignKeyOnJoinedSetWrongValue()
    {

        $set = $this->getForeignSet();
        $set->fk();

        $linkedRecord = $set->fk->newRecord();
        $linkedRecord->name = 'joined';
        $this->assertTrue($linkedRecord->save());

        $record = $set->newRecord();
        $record->name = 'main record';
        $record->fk = $linkedRecord->id;
        $this->assertFalse($record->save()); // the save method will throw exception because of the missing record

        $getRecord = $set->get($record->id);

        $this->assertSame($getRecord->fk->id, $linkedRecord->id);
    }


    /**
     *
     */
    public function testSavingForeignKeyOnJoinedSetUpdatedElement()
    {

        $set = $this->getForeignSet();

        $record1 = $set->newRecord();
        $record1->name = 'main record';
        $record1->save();

        $set->join('fk');
        $record2 = $set->get($record1->id);

        $record2->fk->name = 'joined1';

        $this->assertTrue($record2->save());
        $this->assertTrue($record2->fk->id > 0, 'after save joined record must be saved');

        $gset = $this->getForeignSet();
        $getRecord = $gset->get($record2->id);

        $this->assertTrue($getRecord->fk > 0, 'linkedRecord1 must be referenced here');
        $this->assertSame((int) $getRecord->fk, (int) $record2->fk->id);

    }


    /**
     *
     */
    public function testSavingForeignKeyOnJoinedSetCreatedElement()
    {

        $set = $this->getForeignSet();
        $set->join('fk');

        $record = $set->newRecord();
        $record->name = 'main record';
        $record->fk->name = 'joined1';

        $this->assertTrue($record->save());
        $this->assertTrue($record->id > 0, 'after save, id must be set');
        $this->assertTrue($record->fk->id > 0, 'after save joined record must be saved');

        $gset = $this->getForeignSet();
        $getRecord = $gset->get($record->id);

        $this->assertTrue($getRecord->fk > 0, 'linkedRecord1 must be referenced here');
        $this->assertSame((int) $getRecord->fk, (int) $record->fk->id);
    }
    
    
    
    
    /**
     *
     */
    public function testSavingForeignKeyOnJoinedSetCreatedIsolatedElement()
    {
        global $babDB;


        $set = $this->getForeignSet();
        $set->join('fk');
    
        $record1 = $set->newRecord();
        $record1->name = 'record 1';
        $record1->fk->name = 'joined 1';
        $record1->save();
    
        $record2 = $set->newRecord();
        $record2->name = 'record 2';
        $record2->fk->name = 'joined 2';
        $record2->save();
        
        
        $this->assertTrue($record1->id != $record2->id, 'after save we must have one id per record');
        $this->assertTrue($record1->fk->id != $record2->fk->id, 'after save we must have one id per record');
    
        $gset = $this->getForeignSet();
        $getRecord1 = $gset->get($record1->id);
        $getRecord2 = $gset->get($record2->id);
    
        $this->assertSame((int) $getRecord1->fk, (int) $record1->fk->id);
        $this->assertSame((int) $getRecord2->fk, (int) $record2->fk->id);
    }
    
    


    /**
     *
     *//*
    public function testSavingForeignKeyOnJoinedSet()
    {

        $set = $this->getForeignSet();
        $set->fk();

        $linkedRecord1 = $set->fk->newRecord();
        $linkedRecord1->name = 'joined1';

        $record = $set->newRecord();
        $record->name = 'main record';
        $record->fk = $linkedRecord1;

        $this->assertTrue($record->save());
        $this->assertTrue($record->id > 0, 'after save, id must be set');
        $this->assertTrue($record->fk->id > 0, 'after save joined record must be saved');

        $gset = $this->getForeignSet();
        $getRecord = $gset->get($record->id);

        $this->assertTrue($getRecord->fk > 0, 'linkedRecord1 must be referenced here');
        $this->assertSame($getRecord->fk, $record->fk->id);
    }*/
}
