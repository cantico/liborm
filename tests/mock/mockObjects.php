<?php



$GLOBALS['babDBHost'] = 'localhost';
$GLOBALS['babDBLogin'] = 'test';
$GLOBALS['babDBPasswd'] = '';
$GLOBALS['babDBName'] = 'test';

/**
 * @ignore
 */
class bab_functionality
{
    /**
     * @return bab_functionality
     */
	public static function get($path, $singleton = true)
	{
	    if ('Rrule' === $path) {
	        require_once dirname(__FILE__).'/../../vendor/ovidentia/librrule/programs/rrule.class.php';
	        return new Func_Rrule();
	    }
	    
        return new bab_functionality();
	}
}


function bab_translate($str)
{
	return $str;
}


/**
 * Returns a singleton of the specified class.
 *
 * @param string $classname
 * @return object
 */
function bab_getInstance($classname)
{
	static $instances = null;
	if (is_null($instances)) {
		$instances = array();
	}
	if (!array_key_exists($classname, $instances)) {
		$instances[$classname] = new $classname();
	}

	return $instances[$classname];
}




/**
 * return non breaking space
 * @return string
 */
function bab_nbsp()
{
    // return this fixed value for test
    return chr(160);
}




/**
 * Returns a unix timestamp corresponding to the string $time formatted as a MYSQL DATETIME
 *
 * @access  public
 *
 * @param   string	$time	(eg. '2006-03-10 17:37:02')
 *
 * @return  int	unix timestamp
 */
function bab_mktime($time)
{
    $arr = explode(" ", $time); //Split days and hours
    if ('0000-00-00' == $arr[0] || '' == $arr[0]) {
        return null;
    }
    $arr0 = explode("-", $arr[0]); //Split year, month et day
    if (isset($arr[1])) { //If the hours exist we send back days and hours
        $arr1 = explode(":", $arr[1]);
        return mktime($arr1[0], $arr1[1], $arr1[2], $arr0[1], $arr0[2], $arr0[0]);
    } else { //If the hours do not exist, we send back only days
        return mktime(0, 0, 0, $arr0[1], $arr0[2], $arr0[0]);
    }
}



/**
 * Returns a string containing the time formatted according to the user's preferences
 *
 * @access  public
 * @return  string	formatted time
 * @param   int	$time	unix timestamp
 * @param   boolean $hour	(true == '17/03/2006',
 *							false == '17/03/2006 10:11'
 */
function bab_shortDate($time, $hour = true)
{
    if (null === $time) {
        return '';
    }
    
    return date('d/m/Y' . ($hour? ' H:i' : ''), $time);
}






require_once dirname(__FILE__) . '/../../programs/orm.class.php';

$ORM = new Func_LibOrm;
$ORM->initMysqlMock();

ORM_MySqlRecordSet::setBackend(new ORM_MySqlMockBackend(new LibOrm_MysqlMockAdapter));


/**
 *
 * @property ORM_PkField 		$id
 * @property ORM_StringField 	$string
 * @property ORM_TextField 		$text
 * @property ORM_IntField 		$int
 * @property ORM_DecimalField 	$decimal
 * @property ORM_CurrencyField 	$currency
 * @property ORM_EmailField 	$email
 * @property ORM_DateField 		$date
 * @property ORM_DatetimeField 	$bool
 * @property ORM_EnumField 		$enum
 * @property ORM_HtmlField 		$html
 * @property ORM_TimeField 		$time
 * @property ORM_UrlField 		$url
 * @property ORM_UserField 		$user
 */
class ORM_MockSet extends ORM_MySqlRecordSet
{
    /**
     * Mock set
     */
	public function __construct()
	{
		parent::__construct();
	
		$this->setPrimaryKey('id');
	
		$this->addFields(
			ORM_StringField('string'),
			ORM_TextField('text'),
			ORM_IntField('int'),
			ORM_DecimalField('decimal', 3),
			ORM_CurrencyField('currency'),
			ORM_EmailField('email'),
			ORM_DateField('date'),
			ORM_DatetimeField('datetime'),
			ORM_BoolField('bool'),
			ORM_EnumField('enum', array(1 => 'v1', 2 => 'v2')),
			ORM_HtmlField('html'),
			ORM_TimeField('time'),
			ORM_UrlField('url'),
			ORM_UserField('user')
		);

	}
}


/**
 *
 * @property int 			$id
 * @property string 		$string
 * @property string 		$text
 * @property int 			$int
 * @property float 			$decimal
 * @property float 			$currency
 * @property string 		$email
 * @property string 		$date
 * @property bool 			$bool
 * @property int 			$enum
 * @property string 		$html
 * @property string 		$time
 * @property string 		$url
 * @property string 		$user
 */
class ORM_Mock extends ORM_MySqlRecord
{
}





class ORM_TestRecordSet extends ORM_RecordSet
{
}

class ORM_TestRecord extends ORM_Record
{
}
