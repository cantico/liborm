<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * a index key or unique key on a list of field
 *
 */
class ORM_FieldKey
{
    /**
     * List of fields of the key
     * @var ORM_Field[]
     */
    protected $fields = array();
    
    /**
     * If the key is an unique key or else an index
     * @var bool
     */
    public $unique = false;
    
    
    /**
     * Construct a key on fields
     * @param Array $fields
     */
    public function __construct(Array $fields)
    {
        if (empty($fields)) {
            throw new ORM_Exception('At least one field is mandatory to create a key');
        }
        
        $this->fields = $fields;
    }
    
    
    /**
     * Get the name of key
     * @return string
     */
    public function getName()
    {
        $names = array();
        
        foreach($this->fields as $field) {
            /*@var $field ORM_Field */
            $names[] = $field->getName();
        }
        
        return implode('_', $names);
    }
    
    /**
     * @return ORM_Field[]
     */
    public function getFields()
    {
        return $this->fields;
    }
}