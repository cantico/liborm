<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
************************************************************************
* Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
*                                                                      *
* This file is part of Ovidentia.                                      *
*                                                                      *
* Ovidentia is free software; you can redistribute it and/or modify    *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation; either version 2, or (at your option)  *
* any later version.													*
*																		*
* This program is distributed in the hope that it will be useful, but  *
* WITHOUT ANY WARRANTY; without even the implied warranty of			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
* See the  GNU General Public License for more details.				*
*																		*
* You should have received a copy of the GNU General Public License	*
* along with this program; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
* USA.																	*
************************************************************************/

require_once 'vendor/ovidentia/ovidentia/ovidentia/utilit/dbutil.php';

/**
 * Database adapter for the mysql backend
* This class is compatible with babDatabase from ovidentia core
*
* This can be used instead of $babDB for the unit tests of ORM classes
*/
class LibOrm_MysqlMockAdapter extends babDatabase
{
    
    /**
     * Do nothing
     */
	public function __construct()
	{

	}

	/**
	 * Do nothing, no error in mock
	 */
    public function db_print_error($text)
    {

    }

    /**
     * Do nothing, no connect in mock
     */
	public function db_connect()
	{

	}

	/**
	 * Do nothing, no connect in mock
	 */
	public function db_close()
	{

	}



	/**
	 * sends an unique query (multiple queries are not supported)
	 * @param	string	$query
	 * @return	resource|false
	 */
	public function db_query($query)
	{
        return false;
	}

	/**
	 * @return int
	 */
	public function db_num_rows($result)
	{
		return 0;
	}

	/**
	 * Do nothing
	 */
	public function db_fetch_assoc($result)
	{

	}

	/**
	 * Do nothing
	 */
	public function db_affected_rows()
	{
		return 0;
	}

	/**
	 * Do nothing
	 */
	public function db_insert_id()
	{
		return 0;
	}

	/**
	 * Do nothing
	 */
	public function db_data_seek($res, $row)
	{
	}

	/**
	 * Do nothing
	 * @param string $str
	 * @return string
	 */
	public function db_escape_string($str)
	{
		return $str;
	}


	/**
	 * Do nothing
	 */
	public function db_free_result($result)
	{

	}


	/**
	 * Get error info
	 * return false if no error on the last query
	 * return the error string if error on the last query
	 * @since	6.4.95
	 * @return 	false|string
	 */
	public function db_error()
	{

	}


	/**
	 * Get error manager status and enable or disable error manager
	 * @since	6.4.95
	 * @param	boolean	[$status]
	 * @return 	boolean
	 */
	public function errorManager($status = null)
	{

	}

	/**
	 * Query without error manager
	 * @since	6.4.95
	 * @param	string	$query
	 * @return	resource|false
	 */
	public function db_queryWem($query)
	{

	}
}
