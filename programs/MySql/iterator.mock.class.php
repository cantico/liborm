<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/iterator.class.php';

/**
 * return a mysql iterator to iterator in a array of records
 */
class ORM_MySqlMockIterator extends ORM_Iterator implements SeekableIterator, Countable 
{
	private $values = array();
	
	private $valid = false;
	
	/**
	 * @param array $values
	 */
	public function __construct(Array $values)
	{
		$this->values = array_values($values);
	}
	
	
	/**
	 * Return the current element
	 *
	 * @return ORM_Record
	 */
	public function current()
	{
		return current($this->values);
	}
	
	
	/**
	 * Return the key of the current element
	 *
	 * @return integer The key of the current element
	 */
	public function key()
	{
		return key($this->values);
	}
	
	
	/**
	 * Go to the next element
	 */
	public function next()
	{
		$this->valid = (false !== next($this->values));
	}
	
	
	/**
	 * Rewind the Iterator to the first element.
	 */
	public function rewind()
	{
	    // apply orders
	    
	    
	    if (!empty($this->aOrder))
	    {
	       usort($this->values, array($this, 'sortMethod'));
	    }
	    
		$this->valid = (false !== reset($this->values));
	}
	
	/**
	 * @param ORM_Record $record1
	 * @param ORM_Record $record2
	 * 
	 * @return int
	 */
	private function sortMethod($record1, $record2) {
	    
	    foreach($this->aOrder as $arr)
	    {
	        $fieldname = $arr[0]->getName();
	        $direction = 'DESC' === $arr[1] ? -1 : 1;
	        
	        $v1 = $record1->$fieldname;
	        $v2 = $record2->$fieldname;
	        
	        if ($v1 === $v2)
	        {
	            continue;
	        }
	        
	        if ($v1 > $v2)
	        {
	            return $direction;
	        }
	        
	        return (-1*$direction);
	    }
	    
	    
	    return 0;
	    
	}
	
	
	/**
	 * Place the Iterator on the $iRowNumberth element, starting at 0.
	 *
	 * @param int $iRowNumber
	 */
	public function seek($iRowNumber)
	{
		$this->rewind();
		$found = false;
		while ($this->key() !== $iRowNumber) {
			$this->next();
			$found = true;
		}
		
		$this->valid = $found;
	}
	
	
	/**
	 * Check if there is a current element after calls to rewind(), seek() or next().
	 *
	 * @return bool True if there is a current element after calls to rewind(), seek() or next()
	 */
	public function valid()
	{
		return $this->valid;
	}
	
	
	/**
	 * Returns the number of elements in the iterator.
	 *
	 * @return int
	 */
	public function count()
	{
		return count($this->values);
	}
	
	
    /**
     * @return string
     */
	public function getSelectQuery()
	{
		return 'Mock query for tests';
	}
	
	
	
	/**
	 * Set an ascending orderby field to the iterator.
	 *
	 * @param ORM_Field $oField
	 * @return ORM_Iterator
	 */
	public function orderAsc(ORM_Field $oField)
	{
	    $this->aOrder = array();
	    return parent::orderAsc($oField);
	}
	
	/**
	 * Set a descending orderby field to the iterator.
	 *
	 * @param ORM_Field $oField
	 * @return ORM_Iterator
	 */
	public function orderDesc(ORM_Field $oField)
	{
	    $this->aOrder = array();
	    return parent::orderDesc($oField);
	}
}