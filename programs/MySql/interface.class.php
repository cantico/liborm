<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


interface IRelation
{

	/**
	 * Get an ORM_Relation.
	 *
	 * @param	string	$sRelationName
	 *
	 * @return	ORM_Relation	Or null if it does not exist.
	 */
	public function getRelation($sRelationName);
	
	/**
	 * Expands the foreign key identified by $sFkFieldName:
	 * the foreign key field is replaced by an instance of the referenced set.
	 *
	 * @param	string	$sFkFieldName
	 *
	 * @return	mixed	ORM_RecordSet on success, null otherwise
	 */
	public function join($sFkFieldName);
	
	/**
	 * Specifies that the Set is linked to one element of the Set corresponding to $sElementClassName.
	 *
	 * @param string $sFieldName			Field name of the set that have a link
	 * @param string $sForeignSetClassName	Class name of the set on which the field is bound
	 */
	public function hasOne($sFieldName, $sForeignSetClassName);

	/**
	 * Specifies that the Set is linked to N elements of the Set corresponding to $recordClassName.
	 *
	 * @param string $sRelationName
	 * @param string $sForeignSetClassName
	 * @param string $sForeignRelationName
	 * @return ORM_ManyRelation
	 */
	public function hasMany($sRelationName, $sForeignSetClassName, $sForeignRelationName, $sRelationSetClassName = null);
}
